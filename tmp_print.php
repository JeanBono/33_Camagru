<?PHP
function print_user( User $user)
{
	print("This is the content of the User class your gave me:\n");
	print("user_id: " . $user->get_user_id() . PHP_EOL);
	print("user_login: " . $user->get_user_login() . PHP_EOL);
	print("user_email: " . $user->get_user_email() . PHP_EOL);
	print("user_passwd: " . $user->get_user_passwd() . PHP_EOL);
	if ($user->get_user_email_comment())
		print("user_email_comment: true" . PHP_EOL);
	else
		print("user_email_comment: false" . PHP_EOL);
	if ($user->get_user_admin_rights())
		print("user_admin_rights: true" . PHP_EOL);
	else
		print("user_admin_rights: false" . PHP_EOL);
}
function print_pic( Picture $pic)
{
	print("This is the content of the Picture class your gave me:\n");
	print("pic_id: " . $pic->get_pic_id() . PHP_EOL);
	print("pic_url: " . $pic->get_pic_url() . PHP_EOL);
	print("pic_creation_date: " . $pic->get_pic_creation_date() . PHP_EOL);
	print("pic_likes: " . $pic->get_pic_likes() . PHP_EOL);
	print("pic_user_id: " . $pic->get_pic_user_id() . PHP_EOL);
}
function print_com( Comment $com)
{
	print("This is the content of the Comment class your gave me:\n");
	print("com_id: " . $com->get_com_id() . PHP_EOL);
	print("com_content: " . $com->get_com_content() . PHP_EOL);
	print("com_creation_date: " . $com->get_com_creation_date() . PHP_EOL);
	print("com_pic_id: " . $com->get_com_pic_id() . PHP_EOL);
	print("com_user_id: " . $com->get_com_user_id() . PHP_EOL);
}

?>
