<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo $title; ?></title>
	<meta name="description" content="<?php echo $description ?>">
	<meta name="author" content="42">
	<!-- This link lign doesn't work and I can't figure why... -->
	<link rel="stylesheet" href="../css/main.css" type="text/css">
	<style>
		<?php include '../css/main.css'; ?>
	</style>
</head>
<body>
	<div id="header">
		<ul>
		<li><img src="https://i.imgur.com/qwaD6TC.gif" alt="Camagru Logo" height=200em /></li>
		<li><h2>Amazing Camagru</h2></li>
		<?php echo $header; ?>
		<?php echo $my_pref; ?>
		</ul>
	</div>

	<div class="container">
		<div id="main">
			<?php echo $main; ?>
		</div>
	</div>
	<div id="footer">
		<?php echo $footer; ?>
		<p>&copy Interstellar Kitty Team</p>
		<p>Free month on: Interdimensional cable!</p>
	</div>
</body>
</html>
