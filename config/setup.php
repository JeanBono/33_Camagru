<?php
	include(__DIR__ . "/database.php");

	try {
		$dbh = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
	} catch (PDOException $e) {
		print "Error!: " . $e->getMessage() . PHP_EOL;
		die();
	}

	try {
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$dbh->beginTransaction();
		
		$stmt = $dbh->exec($switch_database);

		$stmt = $dbh->exec($create_table_users);
		$stmt = null;
		$stmt = $dbh->exec($create_table_pictures);
		$stmt = null;
		$stmt = $dbh->exec($create_table_comments);
		$stmt = null;

		$dbh->commit();
		print "3 tables created into camagru database: users / pictures / comments." . PHP_EOL;

	} catch (Exception $e) {
		$dbh->rollBack();
		print "Failed to init Camagru database: " . $e->getMessage() . PHP_EOL;
	}

	$dbh = null;
?>
