<?php
	$DB_DSN = 'mysql:host=localhost;dbname=camagru';
	$DB_USER = "camagru_loves_databases";
	$DB_PASSWORD = "jean-michel-poireau";
//	$DB_PASSWORD = "*B3E58E093C10D225BA6CD97A4BA760139B6F8274";

	$switch_database = "USE camagru;";
	$create_table_users = "CREATE TABLE `users` (
	`user_id` INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	`user_login` VARCHAR(30) NOT NULL,
	`user_email` VARCHAR(30) NOT NULL,
	`user_passwd` VARCHAR(30) NOT NULL,
	`user_email_comment` BOOLEAN DEFAULT 1 NOT NULL,
	`user_admin_rights` BOOLEAN DEFAULT 0 NOT NULL);";
	$create_table_pictures = "CREATE TABLE `pictures` (
	`pic_id` INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	`pic_url` VARCHAR(512) NOT NULL,
	`pic_creation_date` TIMESTAMP NOT NULL,
	`pic_likes` INT UNSIGNED,
	`pic_user_id` INT UNSIGNED NOT NULL,
	CONSTRAINT `fk_pic_user_id`
		FOREIGN KEY (pic_user_id) REFERENCES users (user_id)
		ON UPDATE RESTRICT);";
	$create_table_comments = "CREATE TABLE `comments` (
	`com_id` INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	`com_content` VARCHAR(280) NOT NULL,
	`com_creation_date` TIMESTAMP NOT NULL,
	`com_pic_id` INT UNSIGNED NOT NULL,
	CONSTRAINT `fk_com_pic_id`
		FOREIGN KEY (com_pic_id) REFERENCES pictures (pic_id)
		ON UPDATE RESTRICT,
	`com_user_id` INT UNSIGNED NOT NULL,
	CONSTRAINT `fk_com_user_id`
		FOREIGN KEY (com_user_id) REFERENCES users (user_id)
		ON UPDATE RESTRICT);";

	$drop_all_tables = "DROP TABLE comments;
	DROP TABLE pictures;
	DROP TABLE users;";
?>
