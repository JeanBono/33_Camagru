<?php

function create_user( $dbh, $user_login, $user_email, $user_passwd )
{
	require_once(__DIR__ . "/User.class.php");

	require_once(__DIR__ . "/../tmp_print.php");

	$user_specs = array(
		"user_login" => $user_login,
		"user_email" => $user_email,
		"user_passwd" => $user_passwd,
	);

	$new_user = new User( $user_specs );

	$command = "INSERT INTO users SET user_login = '" . $new_user->get_user_login() . "', " .
									"user_email = '" . $new_user->get_user_email() . "', " .
									"user_passwd = '" . $new_user->get_user_passwd() . "';";

	$new_user->__destruct();

	try {
		$dbh->query($command);
		$new_user = find_user( $dbh, $user_login );
	} catch (PDOException $e) {
		print "Error: " . $e->getMessage() . PHP_EOL;
		return false;
	}
	return $new_user;
}

function find_user( $dbh, $user_login )
{
	require_once(__DIR__ . "/User.class.php");

	try {
		$query = "
			SELECT *
			FROM users
			WHERE user_login = '$user_login';";

		$user = false;
		$count = 0;
		foreach( $dbh->query($query) as $element)
		{
			$count += 1;
			if ($count > 1)
			{
				print("Error: two accounts with same login inside users table.\n");
				return false;
			}
			$user = new User( $element );
			$user->set_user_email_comment( $element['user_email_comment'] );
			$user->set_user_admin_rights( $element['user_admin_rights'] );
		}
		if ($user === false)
			print("Error: no such user inside database\n");
		return $user;
	} catch (PDOException $e) {
		print "Error: " . $e->getMessage() . PHP_EOL;
		return false;
	}
}
?>
