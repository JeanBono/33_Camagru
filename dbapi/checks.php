<?php

error_reporting(E_STRICT);

function is_a_valid_id($input)
{
	if (filter_var( $input, FILTER_VALIDATE_INT ) && $input > 0)
		return true;
	else
		return false;	
}

function is_a_boolean($input) {
	if (filter_var( $input, FILTER_VALIDATE_BOOLEAN ) !== NULL ) {
		return true;
	}
	else {
		return false;
	}
}

function is_a_valid_password($input) {

	if ($input === false
		|| $input === null
		|| empty($input))
		return false;

	/*
	 * Check between the start and the end of the password if:
	 *  - The string contains at least one digit.
	 *  - The string contains at least one Letter (Upper/Lower)
	 *  - The set have digit, alpha, and special chars.
	 *  - The string lenght is between [6;30] range.
	 * */
	if (!preg_match('/^(?=.*\d)(?=.*[@#\-_$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_$%^&+=§!\?]{8,20}$/',$input)) {
		return false;
	}
	return true;
}

function is_a_valid_login($input) {
	if ($input === false
		|| $input === null
		|| empty($input))
		return false;

	$username = filter_var($input, FILTER_SANITIZE_STRING);

    /*
     * The username should be in lower/upper alpha-num range,
     * lenght between 5 and 20.
     */
	if (!preg_match('/^[a-zA-Z0-9]{5,20}$/', $username)) {
		return false;
	}
	return true;
}

?>
