<?PHP
	require_once(__DIR__ . "/create_user.php");
	require_once(__DIR__ . "/User.class.php");

	require_once(__DIR__ . "/../tmp_print.php");

	require_once(__DIR__ . "/database_connection.php");

	User::$verbose = true;

	$dbh = database_connection();
	if ($dbh === false)
		die();

	$user = find_user($dbh, "toto");

	if ($user === false)
		die();

	print_user($user);

	$user = create_user( $dbh, "titi", "titi@toto.com", "titiaimelepoulet" );

	if ($user === false)
		die();

	print_user($user);

?>
