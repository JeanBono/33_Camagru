<?php

include_once(__DIR__ . "/checks.php");

Class User {
	public static $verbose = false;
	private $_user_id = -1;
	private $_user_login = null;
	private $_user_email = null;
	private $_user_passwd = null;
	private $_user_email_comment = true;
	private $_user_admin_rights = false;


	private function _error_input( $mess = "" )
	{
		if (self::$verbose)
			print("Can't create User class. There is an input error.\n" . $mess . PHP_EOL);
		return false;
	}

	public static function doc()
	{
		if (($file = file_get_contents(__DIR__ . 'User.doc.txt')) === false)
		{
			if (self::$verbose)
				print("Error. Can't read User class doc file\n");
			return false;
		}
		else
			return ($file);
	}

	public function __construct( array $kwargs )
	{

		if (is_array($kwargs) === true
			&& isset($kwargs['user_login'])
			&& isset($kwargs['user_email'])
			&& isset($kwargs['user_passwd']))
		{
			self::set_user_login($kwargs['user_login']);
			self::set_user_email($kwargs['user_email']);
			self::set_user_passwd($kwargs['user_passwd']);
			if (isset($kwargs['user_id']))
			{
				self::set_user_id($kwargs['user_id']);
				if (self::$verbose)
					print("New User class with login " . self::get_user_login() . " constructed.\n");
			}
			else if (self::$verbose)
				print("New User class with login " . self::get_user_login() . " constructed without ID.\n");
		}
		else
		{
			if (self::$verbose)
				print("Wrong input. Can't create User object.\n");
			return false;
		}
	}

	public function __destruct()
	{
		if (self::$verbose)
			print("User class with login " . self::get_user_login() . " destructed.\n");
		return false;
	}

	public function get_user_id()
	{
		return $this->_user_id;
	}
	public function get_user_login()
	{
		return $this->_user_login;
	}
	public function get_user_email()
	{
		return $this->_user_email;
	}
	public function get_user_passwd()
	{
		return $this->_user_passwd;
	}
	public function get_user_email_comment()
	{
		return $this->_user_email_comment;
	}
	public function get_user_admin_rights()
	{
		return $this->_user_admin_rights;
	}

	public function set_user_id( $in_id )
	{
		if ( $this->_user_id !== -1 )
		{
			if (self::verbose)
				print("Error: you can't modify an user_id if it was already set.\n");
			return false;
		}
		if ( is_a_valid_id($in_id) === false )
		{
			if (self::verbose)
				print("Error: this id isn't a valid value.\n");
			return false;
		}
		$this->_user_id = $in_id;
	}
	public function set_user_login( $in_login )
	{
		$this->_user_login = $in_login;
	}
	public function set_user_email( $in_email )
	{
		$this->_user_email = $in_email;
	}
	public function set_user_passwd( $in_passwd )
	{
		$this->_user_passwd = $in_passwd;
	}
	public function set_user_email_comment( $in_email_comment )
	{
		if (is_a_boolean( $in_email_comment ) === false)
		{
			self::_error_input( "Error with email_comment setter" );
			return false;
		}
		$this->_user_email_comment = $in_email_comment;
	}
	public function set_user_admin_rights( $in_admin_rights )
	{
		if (is_a_boolean( $in_admin_rights ) === false)
		{
			self::_error_input( "Error with admin_rights setter" );
			return false;
		}
		$this->_user_admin_rights = $in_admin_rights;
	}

}
?>
