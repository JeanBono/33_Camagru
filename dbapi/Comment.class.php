<?php

include_once(__DIR__ . "/checks.php");

Class Comment {
	public static $verbose = false;
	private $_com_id = -1;
	private $_com_content = null;
	private $_com_creation_date = null;
	private $_com_pic_id = -1;
	private $_com_user_id = -1;


	private function _error_input( $mess = "" )
	{
		if (self::$verbose)
			print("Can't create Comment class. There is an input error.\n" . $mess . PHP_EOL);
		return false;
	}

	public static function doc()
	{
		if (($file = file_get_contents(__DIR__ . 'Comment.doc.txt')) === false)
		{
			if (self::$verbose)
				print("Error. Can't read Comment class doc file\n");
			return false;
		}
		else
			return ($file);
	}

	public function __construct( array $kwargs )
	{
		if (is_array($kwargs) === true
			&& isset($kwargs['com_content'])
			&& isset($kwargs['com_creation_date'])
			&& isset($kwargs['com_pic_id'])
			&& isset($kwargs['com_user_id']))
		{
			self::set_com_content($kwargs['com_content']);
			self::set_com_creation_date($kwargs['com_creation_date']);
			self::set_com_pic_id($kwargs['com_pic_id']);
			self::set_com_user_id($kwargs['com_user_id']);
			if (isset($kwargs['com_id']))
			{
				self::set_com_id($kwargs['com_id']);
				if (self::$verbose)
					print("New Comment object constructed with ID.\n");
			}
			else if (self::$verbose)
				print("New Comment object constructed without ID.\n");
		}
		else
		{
			if (self::$verbose)
				print("Wrong input. Can't create Comment object.\n");
			return false;
		}
	}

	public function __destruct()
	{
		if (self::$verbose)
			print("Comment class with content:\n->" . self::get_com_content() . "<-\ndestructed.\n");
		return false;
	}

	public function get_com_id()
	{
		return $this->_com_id;
	}
	public function get_com_content()
	{
		return $this->_com_content;
	}
	public function get_com_creation_date()
	{
		return $this->_com_creation_date;
	}
	public function get_com_pic_id()
	{
		return $this->_com_pic_id;
	}
	public function get_com_user_id()
	{
		return $this->_com_user_id;
	}

	public function set_com_id( $in_id )
	{
		if ( $this->_com_id !== -1 )
		{
			if (self::verbose)
				print("Error: you can't modify a com_id if it was already set.\n");
			return false;
		}
		if ( is_a_valid_id($in_id) === false )
		{
			if (self::verbose)
				print("Error: this id isn't a valid value.\n");
			return false;
		}
		$this->_com_id = $in_id;
	}
	public function set_com_content( $in_content )
	{
		$this->_com_content = $in_content;
	}
	public function set_com_creation_date( $in_creation_date )
	{
		$this->_com_creation_date = $in_creation_date;
	}
	public function set_com_pic_id( $in_pic_id )
	{
		if ( $this->_com_pic_id !== -1 )
		{
			if (self::verbose)
				print("Error: you can't modify a com_pic_id if it was already set.\n");
			return false;
		}
		if ( is_a_valid_id($in_pic_id) === false )
		{
			if (self::verbose)
				print("Error: this pic_id isn't a valid value.\n");
			return false;
		}
		$this->_com_pic_id = $in_pic_id;
	}
	public function set_com_user_id( $in_user_id )
	{
		if ( $this->_com_user_id !== -1 )
		{
			if (self::verbose)
				print("Error: you can't modify a com_user_id if it was already set.\n");
			return false;
		}
		if ( is_a_valid_id($in_user_id) === false )
		{
			if (self::verbose)
				print("Error: this user_id isn't a valid value.\n");
			return false;
		}
		$this->_com_user_id = $in_user_id;
	}
}
?>
