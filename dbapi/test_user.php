<?PHP
	require_once(__DIR__ . "/user_api.php");

	require_once(__DIR__ . "/User.class.php");

	require_once(__DIR__ . "/../tmp_print.php");

	require_once(__DIR__ . "/database_connection.php");

	User::$verbose = true;

	$dbh = database_connection();
	if ($dbh === false)
		die();

	$user = find_user($dbh, 3);

	if ($user === false)
		die();

	print_user($user);

	print(PHP_EOL . PHP_EOL);

	$user = create_user( $dbh, "titi", "titi@toto.com", "titiaimelepoulet", true, false );

	print_user($user);

	print(PHP_EOL . PHP_EOL);

	$user->set_user_passwd("mdeville");

	update_user( $dbh, $user );

	print_user($user);

	print(PHP_EOL . PHP_EOL);

	delete_user( $dbh, $user );
/*
 * Nick and password checker unit tests
 */
// var_dump (is_a_valid_password("Foobar75017!@"));
// var_dump (is_a_valid_password("fueofe223123u"));
// var_dump (is_a_valid_password("fueof1E23eu122232232323671238612783612378612387612387162387163123871623781628371623871231231728937891237129837192837918237912379123719237192837219832198371298312981-"));
// var_dump (is_a_valid_password("-eEofeu"));
// var_dump (is_a_valid_password("fG"));
// var_dump (is_a_valid_password("p4ssW0rd!"));
// var_dump (is_a_valid_password("e879o6988986idegt@#"));
// var_dump (is_a_valid_password("eu78oeu9oko]eoefoe&*&^*&^@29Hu"));
// var_dump (is_a_valid_password("d00md00mb210101$%%!@"));
// var_dump (is_a_valid_password("Flu77yP4sS1337#$"));

// var_dump (is_a_valid_login("bamboul33t"));
// var_dump (is_a_valid_login("11ueee"));
// var_dump (is_a_valid_login("poulettte"));
// var_dump (is_a_valid_login("k99keehe"));
// var_dump (is_a_valid_login("hophop"));
// var_dump (is_a_valid_login("a"));
// var_dump (is_a_valid_login("eoe"));
// var_dump (is_a_valid_login("2sor"));
// var_dump (is_a_valid_login("GD!@TG#!TGF@"));
// var_dump (is_a_valid_login("<script>alert(document.cookie)</script>"));

?>
