<?php

function delete_pic( $dbh, Picture $pic )
{
	require_once(__DIR__ . "/Picture.class.php");

	if ($pic->get_pic_id() === -1)
	{
		if (Picture::$verbose)
			print("Error: you can't delete a Picture who has not an id coherent with the database.\n");
		return false;
	}

	$query = "DELETE FROM pictures
		WHERE pic_id='" . $pic->get_pic_id() . "';";

	try {
		 $dbh->query($query);
		if (Picture::$verbose)
			print("Picture with url: " . $pic->get_pic_url() . " successfully deleted\n");
		return true;
	} catch (PDOException $e) {
		if (Picture::$verbose)
			print "Error: " . $e->getMessage() . PHP_EOL ;
		return false;
	}
}

function research_pic_collision( $dbh, Picture $pic )
{
	require_once(__DIR__ . "/Picture.class.php");

	try {
		$query = "
			SELECT *
			FROM pictures
			WHERE pic_url = '" . $pic->get_pic_url() . "';";

		foreach ($dbh->query($query) as $element)
		{
			if ( $pic->get_pic_id() !== $element['pic_id'] )
				return true;
		}
	} catch (PDOException $e) {
		if (Picture::$verbose)
			print("Error: " . $e->getMessage() . PHP_EOL);
		return false;
	}
	return NULL;
}

function update_pic( $dbh, Picture $pic )
{
	require_once(__DIR__ . "/Picture.class.php");

	if ($pic->get_pic_id() === -1)
	{
		if (Picture::$verbose)
			print("Error: you can't update a Picture who has not an id coherent with the database.\n");
		return false;
	}

	if ( research_pic_collision( $dbh, $pic ) !== NULL )
	{
		if (Picture::$verbose)
			print("Error: Can't update a Picture if its url is already in use.\n");
		return false;
	}

	$query = "UPDATE pictures SET 
		pic_url='". $pic->get_pic_url() . "', " .
		"pic_creation_date= FROM_UNIXTIME(" . $pic->get_pic_creation_date() . "), " .
		"pic_likes='" . $pic->get_pic_likes() . "', " .
		"pic_user_id='" . $pic->get_pic_user_id() . "' 
		WHERE pic_id='" . $pic->get_pic_id() . "';";

	try {
		 $dbh->query($query);
		if (Picture::$verbose)
			print("Picture with url: " . $pic->get_pic_url() ." successfully updated\n");
		return true;
	} catch (PDOException $e) {
		if (Picture::$verbose)
			print "Error: " . $e->getMessage() . PHP_EOL ;
		return false;
	}
}

function acquire_pic_id( $dbh, Picture $pic )
{
	require_once(__DIR__ . "/Picture.class.php");

	try {
		$query = "
			SELECT *
			FROM pictures
			WHERE pic_url = '" . $pic->get_pic_url() . "';";

		$count = 0;
		foreach( $dbh->query($query) as $element )
		{
			$count += 1;
			if ($count > 1)
			{
				if (Picture::$verbose)
					print("Error: two pictures with same url inside pictures table.\n");
				return false;
			}
			if (isset($element['pic_id']) === false)
			{
				if (Picture::$verbose)
					print("Error: Can't find element Picture id inside database.\n");
				return false;
			}
			$pic->set_pic_id($element['pic_id']);
		}
		if ($count)
			return true;
		else
			return false;
	} catch (PDOException $e) {
		if (Picture::$verbose)
			print "Error: " . $e->getMessage() . PHP_EOL ;
		return false;
	}
}

function there_is_a_pic_match( $dbh, $pic_url )
{
	try {
		$query = "
			SELECT *
			FROM pictures
			WHERE pic_url = '" . $pic_url . "';";
		foreach ($dbh->query($query) as $element)
			return true;
	} catch (PDOException $e) {
		if (Picture::$verbose)
			print "Error: " . $e->getMessage() . PHP_EOL ;
		return false;
	}
	return NULL;
}

function create_pic( $dbh, $pic_url, $pic_creation_date, $pic_user_id )
{
	require_once(__DIR__ . "/Picture.class.php");

	$pic_specs = array(
		"pic_url" => $pic_url,
		"pic_creation_date" => $pic_creation_date,
		"pic_user_id" => $pic_user_id);

	if ( ($ret = there_is_a_pic_match($dbh, $pic_url)) !== NULL )
	{
		if (Picture::$verbose)
			print("Error: Can't create a Picture if its url is already in use.\n");
		return false;
	}

	$new_pic = new Picture( $pic_specs );

	if ($new_pic === false)
		return false;

	$command = "INSERT INTO pictures SET pic_url = '" . $new_pic->get_pic_url() . "', " .
				"pic_creation_date = FROM_UNIXTIME(" . $new_pic->get_pic_creation_date() . "), " .
				"pic_user_id = '" . $new_pic->get_pic_user_id() . "';";
	try {
		$dbh->query($command);
		if (acquire_pic_id( $dbh, $new_pic ) === false)
			return false;
		if (Picture::$verbose)
			print("Picture correctly inserted into database.\n");
	} catch (PDOException $e) {
		if (Picture::$verbose)
			print "Error: " . $e->getMessage() . PHP_EOL;
		return false;
	}
	return $new_pic;
}

function find_pic( $dbh, $pic_id )
{
	require_once(__DIR__ . "/Picture.class.php");

	Picture::$verbose = true;

	try {
		$query = "
			SELECT *
			FROM pictures
			WHERE pic_id = $pic_id;";

		$pic = false;
		foreach( $dbh->query($query) as $element)
		{
			if (isset($element['pic_creation_date']))
				$element['pic_creation_date'] = strtotime($element['pic_creation_date']);
			$pic = new Picture( $element );
		}
		if ($pic === false && Picture::$verbose)
			print("Error: no such picture inside database\n");
		return $pic;
	} catch (PDOException $e) {
		if (Picture::$verbose)
			print "Error: " . $e->getMessage() . PHP_EOL;
		return false;
	}
}
?>
