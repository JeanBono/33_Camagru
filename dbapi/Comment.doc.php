<- User ------------------------------------------------------------------------

The User class corresponds to the users table inside our camagru database.

An instance might take not-escaped strings as input, it will make the necessary
escaping.
However, this class is really strict about the type of input and their number.

Expected inputs:
 - $kwargs['user_login']: a string of maximum 30 characters.
 - $kwargs['user_login']: a string of maximum 30 characters.
 - $kwargs['user_login']: a string of maximum 30 characters.
 - $kwargs['user_email_comment']: a boolean.
 - $kwargs['user_admin_rights']: a boolean.

If one of those conditions isn't respected, false will be returned.

Verbose option:
The public attribute $verbose might be set to true in order to activate a
lot of verbose outputs. It is set to false by default.

Available methods:
// getters //
 - User get_user_login();
 - User get_user_email();
 - User get_user_mdp();
 - User get_user_email_comment();
 - User get_user_admin_rights();

// setters //
 - User set_user_login( $in_login );
 - User set_user_email( $in_email );
 - User set_user_mdp( $in_mdp );
 - User set_user_email_comment( $in_email_comment );
 - User set_user_admin_rights( $in_admin_rights );

// basics //
 - User doc();
 - User __construct( array $kwargs );
 - User __destruct();

There isn't a toHtml() method for the User class.

------------------------------------------------------------------------ User ->
