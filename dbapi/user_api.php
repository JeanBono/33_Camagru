<?php

function delete_user( $dbh, User $user )
{
	require_once(__DIR__ . "/User.class.php");

	if ($user->get_user_id() === -1)
	{
		if (User::$verbose)
			print("Error: you can't delete an User who has not an id coherent with the database.\n");
		return false;
	}

	$query = "DELETE FROM users
		WHERE user_id='" . $user->get_user_id() . "';";

	try {
		 $dbh->query($query);
		if (User::$verbose)
			print("User successfully deleted\n");
		return true;
	} catch (PDOException $e) {
		if (User::$verbose)
			print "Error: " . $e->getMessage() . PHP_EOL ;
		return false;
	}
}

function research_user_collision( $dbh, User $user )
{
	require_once(__DIR__ . "/User.class.php");

	try {
		$query = "
			SELECT *
			FROM users
			WHERE user_login = '" . $user->get_user_login() . "';";
		foreach ($dbh->query($query) as $element)
		{
			if ( $user->get_user_id() !== $element['user_id'] )
				return 1;
		}
	} catch (PDOException $e) {
		if (User::$verbose)
			print "Error: " . $e->getMessage() . PHP_EOL ;
		return false;
	}
	try {
		$query = "
			SELECT *
			FROM users
			WHERE user_email = '" . $user->get_user_email() . "';";
		foreach ($dbh->query($query) as $element)
		{
			if ( $user->get_user_id() !== $element['user_id'] )
				return 2;
		}
	} catch (PDOException $e) {
		if (User::$verbose)
			print "Error: " . $e->getMessage() . PHP_EOL ;
		return false;
	}
	return NULL;
}

function update_user( $dbh, User $user )
{
	require_once(__DIR__ . "/User.class.php");

	if ($user->get_user_id() === -1)
	{
		if (User::$verbose)
			print("Error: you can't update an User who has not an id coherent with the database.\n");
		return false;
	}

	if ( ($ret = research_user_collision( $dbh, $user ) ) !== NULL )
	{
		if (User::$verbose)
		{
			if ($ret === 1)
				print("Error: Can't update a User if its login is already in use.\n");
			else if ($ret === 2)
				print("Error: Can't update a User if its email is already in use.\n");
		}
		return false;
	}

	$query = "UPDATE users SET 
		user_login='". $user->get_user_login() . "', " .
		"user_email='" . $user->get_user_email() . "', " .
		"user_passwd='" . $user->get_user_passwd() . "', " .
		"user_email_comment='" . $user->get_user_email_comment() . "', " .
		"user_admin_rights='" . $user->get_user_admin_rights() . "'
		WHERE user_id='" . $user->get_user_id() . "';";

	try {
		 $dbh->query($query);
		if (User::$verbose)
			print("User successfully updated\n");
		return true;
	} catch (PDOException $e) {
		if (User::$verbose)
			print "Error: " . $e->getMessage() . PHP_EOL ;
		return false;
	}
}

function acquire_user_id( $dbh, User $user )
{
	require_once(__DIR__ . "/User.class.php");

	try {
		$query = "
			SELECT *
			FROM users
			WHERE user_login = '" . $user->get_user_login() . "';";

		$count = 0;
		foreach( $dbh->query($query) as $element )
		{
			$count += 1;
			if ($count > 1)
			{
				if (User::$verbose)
					print("Error: two accounts with same login inside users table.\n");
				return false;
			}
			if (isset($element['user_id']) === false)
			{
				if (User::$verbose)
					print("Error: Can't find element User id inside database.\n");
				return false;
			}
			$user->set_user_id($element['user_id']);
		}
		if ($count)
			return true;
		else
			return false;
	} catch (PDOException $e) {
		if (User::$verbose)
			print "Error: " . $e->getMessage() . PHP_EOL ;
		return false;
	}
}

function there_is_a_match( $dbh, $user_specs )
{
	try {
		$query = "
			SELECT *
			FROM users
			WHERE user_login = '" . $user_specs['user_login'] . "';";
		foreach ($dbh->query($query) as $element)
			return 1;
	} catch (PDOException $e) {
		if (User::$verbose)
			print "Error: " . $e->getMessage() . PHP_EOL ;
		return false;
	}
	try {
		$query = "
			SELECT *
			FROM users
			WHERE user_email = '" . $user_specs['user_email'] . "';";
		foreach ($dbh->query($query) as $element)
			return 2;
	} catch (PDOException $e) {
		if (User::$verbose)
			print "Error: " . $e->getMessage() . PHP_EOL ;
		return false;
	}
	return NULL;
}

function create_user( $dbh, $user_login, $user_email, $user_passwd,
						$user_email_comment = true, $user_admin_rights = false )
{
	require_once(__DIR__ . "/User.class.php");

	$user_specs = array(
		"user_login" => $user_login,
		"user_email" => $user_email,
		"user_passwd" => $user_passwd);

	if ( ($ret = there_is_a_match($dbh, $user_specs)) !== NULL )
	{
		if (User::$verbose)
		{
			if ($ret === 1)
				print("Error: Can't create a User if its login is already in use.\n");
			else if ($ret === 2)
				print("Error: Can't create a User if its email is already in use.\n");
		}
		return false;
	}

	$new_user = new User( $user_specs );

	if ($new_user === false)
		return false;

	$ret = $new_user->set_user_email_comment( $user_email_comment );
	if ($ret === false)
		return false;
	$ret = $new_user->set_user_admin_rights( $user_admin_rights );
	if ($ret === false)
		return false;

	$command = "INSERT INTO users SET user_login = '" . $new_user->get_user_login() . "', " .
								"user_email = '" . $new_user->get_user_email() . "', " .
								"user_passwd = '" . $new_user->get_user_passwd() . "', " .
								"user_email_comment = '" . $new_user->get_user_email_comment() . "', " .
								"user_admin_rights = '" . $new_user->get_user_admin_rights() . "';";

	try {
		$dbh->query($command);
		if (acquire_user_id( $dbh, $new_user ) === false)
			return false;
		if (User::$verbose)
			print("User correctly inserted into database.\n");
	} catch (PDOException $e) {
		if (User::$verbose)
			print "Error: " . $e->getMessage() . PHP_EOL;
		return false;
	}
	return $new_user;
}

function find_user( $dbh, $user_id )
{
	require_once(__DIR__ . "/User.class.php");

	try {
		$query = "
			SELECT *
			FROM users
			WHERE user_id = $user_id;";

		$user = false;
		foreach( $dbh->query($query) as $element)
		{
			$user = new User( $element );
			$user->set_user_email_comment( $element['user_email_comment'] );
			$user->set_user_admin_rights( $element['user_admin_rights'] );
		}
		if ($user === false && User::$verbose)
			print("Error: no such user inside database\n");
		return $user;
	} catch (PDOException $e) {
		if (User::$verbose)
			print "Error: " . $e->getMessage() . PHP_EOL;
		return false;
	}
}
?>
