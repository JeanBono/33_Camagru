<?PHP
	require_once(__DIR__ . "/picture_api.php");

	require_once(__DIR__ . "/Picture.class.php");

	require_once(__DIR__ . "/../tmp_print.php");

	require_once(__DIR__ . "/database_connection.php");

	Picture::$verbose = true;

	$dbh = database_connection();
	if ($dbh === false)
		die();

	$pic = find_pic($dbh, 3);

	if ($pic === false)
		die();

	print_pic($pic);

	print(PHP_EOL . PHP_EOL);

	$pic = create_pic( $dbh, "./bibiaimelepoulet.png", 1529428599, 3);

	print_pic($pic);

	print(PHP_EOL . PHP_EOL);

	$pic->set_pic_likes(200);

	update_pic( $dbh, $pic );

	print_pic($pic);

	print(PHP_EOL . PHP_EOL);

	delete_pic( $dbh, $pic );
?>
