<?php

include_once(__DIR__ . "/checks.php");

Class Picture {
	public static $verbose = false;
	private $_pic_id = -1;
	private $_pic_url = null;
	private $_pic_creation_date = null;
	private $_pic_likes = 0;
	private $_pic_user_id = -1;


	private function _error_input( $mess = "" )
	{
		if (self::$verbose)
			print("Can't create Picture class. There is an input error.\n" . $mess . PHP_EOL);
		return false;
	}

	public static function doc()
	{
		if (($file = file_get_contents(__DIR__ . 'Picture.doc.txt')) === false)
		{
			if (self::$verbose)
				print("Error. Can't read Picture class doc file\n");
			return false;
		}
		else
			return ($file);
	}

	public function __construct( array $kwargs )
	{
		if (is_array($kwargs) === true
			&& isset($kwargs['pic_url'])
			&& isset($kwargs['pic_creation_date'])
			&& isset($kwargs['pic_user_id']))
		{
			self::set_pic_url($kwargs['pic_url']);
			self::set_pic_creation_date($kwargs['pic_creation_date']);
			self::set_pic_user_id($kwargs['pic_user_id']);
			if (isset($kwargs['pic_likes']))
				self::set_pic_likes($kwargs['pic_likes']);
			if (isset($kwargs['pic_id']))
			{
				self::set_pic_id($kwargs['pic_id']);
				if (self::$verbose)
					print("New Picture object constructed with ID.\n");
			}
			else if (self::$verbose)
				print("New Picture object constructed without ID.\n");
		}
		else
		{
			if (self::$verbose)
				print("Wrong input. Can't create Picture object.\n");
			return false;
		}
	}

	public function __destruct()
	{
		if (self::$verbose)
			print("Picture class with url " . self::get_pic_url() . " destructed.\n");
		return false;
	}

	public function get_pic_id()
	{
		return $this->_pic_id;
	}
	public function get_pic_url()
	{
		return $this->_pic_url;
	}
	public function get_pic_creation_date()
	{
		return $this->_pic_creation_date;
	}
	public function get_pic_likes()
	{
		return $this->_pic_likes;
	}
	public function get_pic_user_id()
	{
		return $this->_pic_user_id;
	}

	public function set_pic_id( $in_id )
	{
		if ( $this->_pic_id !== -1 )
		{
			if (self::verbose)
				print("Error: you can't modify a pic_id if it was already set.\n");
			return false;
		}
		if ( is_a_valid_id($in_id) === false )
		{
			if (self::verbose)
				print("Error: this id isn't a valid value.\n");
			return false;
		}
		$this->_pic_id = $in_id;
	}
	public function set_pic_url( $in_url )
	{
		$this->_pic_url = $in_url;
	}
	public function set_pic_creation_date( $in_creation_date )
	{
		$this->_pic_creation_date = $in_creation_date;
	}
	public function set_pic_likes( $in_likes )
	{
		$this->_pic_likes = $in_likes;
	}
	public function set_pic_user_id( $in_pic_user_id )
	{
		if ( $this->_pic_user_id !== -1 )
		{
			if (self::verbose)
				print("Error: you can't modify a pic_user_id if it was already set.\n");
			return false;
		}
		if ( is_a_valid_id($in_pic_user_id) === false )
		{
			if (self::verbose)
				print("Error: this user_id isn't a valid value.\n");
			return false;
		}
		$this->_pic_user_id = $in_pic_user_id;
	}
}
?>
