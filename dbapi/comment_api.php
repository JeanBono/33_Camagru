<?php

function delete_com( $dbh, Comment $com )
{
	require_once(__DIR__ . "/Comment.class.php");

	if ($com->get_com_id() === -1)
	{
		if (Comment::$verbose)
			print("Error: you can't delete a Comment who has not an id coherent with the database.\n");
		return false;
	}

	$query = "DELETE FROM comments
		WHERE com_id='" . $com->get_com_id() . "';";

	try {
		 $dbh->query($query);
		if (Comment::$verbose)
			print("Comment with content:\n->".$com->get_com_content()."<-\nsuccessfully deleted\n");
		return true;
	} catch (PDOException $e) {
		if (Comment::$verbose)
			print "Error: " . $e->getMessage() . PHP_EOL ;
		return false;
	}
}

function update_com( $dbh, Comment $com )
{

	require_once(__DIR__ . "/Comment.class.php");

	if ($com->get_com_id() === -1)
	{
		if (Comment::$verbose)
			print("Error: you can't update a Comment who has not an id coherent with the database.\n");
		return false;
	}

	$query = "UPDATE comments SET 
		com_content='". $com->get_com_content() . "', " .
		"com_creation_date= FROM_UNIXTIME(" . $com->get_com_creation_date() . "), " .
		"com_pic_id='" . $com->get_com_pic_id() . "', " .
		"com_user_id='" . $com->get_com_user_id() . "' 
		WHERE com_id='" . $com->get_com_id() . "';";

	try {
		 $dbh->query($query);
		if (Comment::$verbose)
			print("Comment with content:\n->" . $com->get_com_content() ."<-\nsuccessfully updated\n");
		return true;
	} catch (PDOException $e) {
		if (Comment::$verbose)
			print "Error: " . $e->getMessage() . PHP_EOL ;
		return false;
	}
}

function acquire_com_id( $dbh, Comment $com )
{
	require_once(__DIR__ . "/Comment.class.php");

	try {
		$query = "
			SELECT *
			FROM comments
			WHERE com_content = '" . $com->get_com_content() . "'
			AND com_creation_date = FROM_UNIXTIME(" . $com->get_com_creation_date() . ")
			AND com_pic_id = " . $com->get_com_pic_id() . "
			AND	com_user_id = " . $com->get_com_user_id() . ";";

		foreach( $dbh->query($query) as $element )
		{
			$count += 1;
			if ($count > 1)
			{
				if (Comment::$verbose)
					print("Error: a comment was inserted twice into comments table.\n");
				return false;
			}
			if (isset($element['com_id']) === false)
			{
				if (Comment::$verbose)
					print("Error: Can't find id of this comment inside the database.\n");
				return false;
			}
			$com->set_com_id($element['com_id']);
		}
		if ($count)
			return true;
		else
			return false;
	} catch (PDOException $e) {
		if (Comment::$verbose)
			print "Error: " . $e->getMessage() . PHP_EOL ;
		return false;
	}
}

function there_is_a_com_match( $dbh, $com_content, $com_creation_date, $com_pic_id, $com_user_id )
{
	try {
		$query = "
			SELECT *
			FROM comments
			WHERE com_content = '" . $com_content . "'
			AND com_creation_date = FROM_UNIXTIME($com_creation_date)
			AND com_pic_id = $com_pic_id
			AND	com_user_id = $com_user_id;";

		foreach ($dbh->query($query) as $element)
			return true;
	} catch (PDOException $e) {
		if (Picture::$verbose)
			print "Error: " . $e->getMessage() . PHP_EOL ;
		return false;
	}
	return NULL;
}

function create_com( $dbh, $com_content, $com_creation_date, $com_pic_id, $com_user_id )
{
	require_once(__DIR__ . "/Comment.class.php");


	$com_specs = array(
		"com_content" => $com_content,
		"com_creation_date" => $com_creation_date,
		"com_pic_id" => $com_pic_id,
		"com_user_id" => $com_user_id);

	if (there_is_a_com_match($dbh, $com_content,
			$com_creation_date, $com_pic_id, $com_user_id) !== NULL)
	{
		if (Comment::$verbose)
			print("Error: you sent the same creation request twice for the same comment.\n");
		return false;
	}

	$new_com = new Comment( $com_specs );
	if ($new_com === false)
		return false;

	$command = "INSERT INTO comments SET com_content = '" . $new_com->get_com_content() . "', " .
				"com_creation_date = FROM_UNIXTIME(" . $new_com->get_com_creation_date() . "), " .
				"com_pic_id = '" . $new_com->get_com_pic_id() . "', " .
				"com_user_id = '" . $new_com->get_com_user_id() . "';";

	try {
		$dbh->query($command);
		if (acquire_com_id( $dbh, $new_com ) === false)
			return false;
		if (Comment::$verbose)
			print("Comment correctly inserted into database.\n");
	} catch (PDOException $e) {
		if (Comment::$verbose)
			print "Error: " . $e->getMessage() . PHP_EOL;
		return false;
	}
	return $new_com;
}

function find_com( $dbh, $com_id )
{
	require_once(__DIR__ . "/Comment.class.php");

	Comment::$verbose = true;

	try {
		$query = "
			SELECT *
			FROM comments
			WHERE com_id = $com_id;";

		$com = false;
		foreach( $dbh->query($query) as $element)
		{
			if (isset($element['com_creation_date']))
				$element['com_creation_date'] = strtotime($element['com_creation_date']);
			$com = new Comment( $element );
		}
		if ($com === false && Comment::$verbose)
			print("Error: no such comment inside database\n");
		return $com;
	} catch (PDOException $e) {
		if (Comment::$verbose)
			print "Error: " . $e->getMessage() . PHP_EOL;
		return false;
	}
}
?>
