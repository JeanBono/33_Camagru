<?php
function database_connection()
{
	require(__DIR__ . "/../config/database.php");

	try {
		$dbh = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return ($dbh);
	} catch (PDOException $e) {
		print "Error!: " . $e->getMessage() . PHP_EOL;
		return (false);
	}
}
?>
